<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css"/>
    <title><?php echo $p['title'] ?></title>
</head>

<body>

<h1>Knightlik</h1>

<?php $messages = $s->pop(\Knightlik\Knightlik\Util\Session::MESSAGES_ARRAY_NAME, array()) ?>

<?php $class = 'messages' ?>
<?php include '_htmlMessageList.php' ?>


<?php echo $p['content'] ?>

</body>
</html>
