<form method="post">

    <?php $messages = isset($p['errors']['name']) ? $p['errors']['name'] : array() ?>
    <?php $class = 'errors' ?>
    <?php include '_htmlMessageList.php' ?>

    <label for="tournamentName">Nazwa Turnieju: </label>
    <input id="tournamentName" name="tournament[name]" type="text" required="required" placeholder="Nazwa Turnieju">

    <?php $messages = isset($p['errors']['team_amount']) ? $p['errors']['team_amount'] : array() ?>
    <?php $class = 'errors' ?>
    <?php include '_htmlMessageList.php' ?>

    <label for="tournamentTeams">Ilość Drużyn: </label>
    <select name=tournament[team_amount] required="required">
        <option value="8">8</option>
        <option value="16">16</option>
        <option value="32">32</option>
        <option value="64">64</option>
        <option value="128">128</option>
    </select>

    <input  type="submit" value="Dodaj Turniej" name="start"/>
</form>

<a href="http://php.localhost/training-plain-php" class="linkOfButton">
    <button class="button" id="backToList">Wróć do menu</button>
</a>
<br/>