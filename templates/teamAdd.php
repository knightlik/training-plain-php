<h2>Dodaj drużynę</h2>
<form method="post">
    Drużyna:
    <ul>
        <li>
            <?php $messages = isset($p['errors']['name']) ? $p['errors']['name'] : array() ?>
            <?php $class = 'errors' ?>
            <?php include '_htmlMessageList.php' ?>

            <label for="teamName">Nazwa drużyny</label>
            <input id="teamName" name="team[name]" type="text" required="required" placeholder="Nazwa drużyny" value="<?php echo isset($p['data']['name']) ? $p['data']['name'] : '' ?>">
        </li>
        <li>
            <?php $messages = isset($p['errors']['tag']) ? $p['errors']['tag'] : array() ?>
            <?php $class = 'errors' ?>
            <?php include '_htmlMessageList.php' ?>

            <label for="teamTag">Tag</label>
            <input id="teamTag" name="team[tag]" type="text" required="required" placeholder="Tag" value="<?php echo isset($p['data']['tag']) ? $p['data']['tag'] : '' ?>">
        </li>
    </ul>
        Kapitan:
        <ul>
            <li>
                <?php $messages = isset($p['errors']['leaderFirstName']) ? $p['errors']['leaderFirstName'] : array() ?>
                <?php $class = 'errors' ?>
                <?php include '_htmlMessageList.php' ?>

                <label for="teamLeaderFirstName">Imię</label>
                <input id="teamLeaderFirstName" name="team[leaderFirstName]" type="text" required="required" placeholder="Imię" value="<?php echo isset($p['data']['leaderFirstName']) ? $p['data']['leaderFirstName'] : '' ?>">
            </li>
            <li>
                <?php $messages = isset($p['errors']['leaderLastName']) ? $p['errors']['leaderLastName'] : array() ?>
                <?php $class = 'errors' ?>
                <?php include '_htmlMessageList.php' ?>

                <label for="teamLeaderLastName">Nazwisko</label>
                <input id="teamLeaderLastName" name="team[leaderLastName]" type="text" required="required" placeholder="Nazwisko" value="<?php echo isset($p['data']['leaderLastName']) ? $p['data']['leaderLastName'] : '' ?>">
            </li>
            <li>
                <?php $messages = isset($p['errors']['leaderNickName']) ? $p['errors']['leaderNickName'] : array() ?>
                <?php $class = 'errors' ?>
                <?php include '_htmlMessageList.php' ?>

                <label for="teamLeaderNickName">Nick</label>
                <input id="teamLeaderNickName" name="team[leaderNickName]" type="text" required="required" placeholder="Nick" value="<?php echo isset($p['data']['leaderNickName']) ? $p['data']['leaderNickName'] : '' ?>">
        </ul>

    <button class="button" type="submit">Dodaj</button>
</form>
<a href="http://php.localhost/training-plain-php" class="linkOfButton">
    <button class="button" id="backToList">Wróć do menu</button>
</a>
<br/>
