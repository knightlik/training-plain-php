<?php if(!empty($messages)): ?>
    <ul class="<?php echo $class ?>">
        <?php foreach ($messages as $message): ?>
            <li><?php echo $message ?></li>
        <?php endforeach ?>
    </ul>
<?php endif ?>
