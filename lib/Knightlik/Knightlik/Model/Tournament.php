<?php

namespace Knightlik\Knightlik\Model;

use Knightlik\Knightlik\Util\Database;
use PDO;
use PDOException;

class Tournament
{
    /**
     * @var integer
     */
    protected $id = null;
    /**
     * @var string
     */
    protected $name;
    /**
     * @var boolean
     */
    protected $status;
    /**
     * @var integer
     */
    protected $teamAmount;

    /**
     * @param $addtotournament
     * @param $teamtotournamentid
     */
    public static function addTeamInTournament($addtotournament, $teamtotournamentid) {
        try {
            $pdo = Database::getInstance()->getConnection();
            $stmt = $pdo->prepare ("INSERT INTO team_tournament(team_id,tournament_id) VALUES(:team_id, :tournament_id)");

            $stmt->bindValue(':tournament_id', $addtotournament, PDO::PARAM_STR);
            $stmt->bindValue(':team_id', $teamtotournamentid, PDO::PARAM_INT);

            $stmt->execute();
        } catch (PDOException $err) {
            echo 'Połączenie nieudane: ' . $err->getMessage();
        }
    }

    /**
     * @param $id
     * @return Tournament|mixed
     */
    public static function fetchTournament($id)
    {
        try {
            $pdo = Database::getInstance()->getConnection();

            $stmt = $pdo->prepare("SELECT * FROM tournament WHERE id=:id");
            $stmt->bindValue(':id', $id, PDO::PARAM_INT);

            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_ASSOC);

            $tournament = $stmt->fetch();
            //TODO:sprawdź czy null
            $tournament = new Tournament($tournament);
        } catch (PDOException $err) {
            echo 'Połączenie nieudane: ' . $err->getMessage();
        }

        return $tournament;
    }

    /**
     * @param $id
     * @return int
     */
    public static function findAllTeams($id)
    {
        try {
            $pdo = Database::getInstance()->getConnection();

            $stmt = $pdo->prepare("SELECT * FROM `team_tournament` WHERE tournament_id=:id");
            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            $countTeam = $stmt->rowCount();


        } catch (PDOException $err) {
            echo 'Połączenie nieudane: ' . $err->getMessage();
        }

        return $countTeam;
    }

    /**
     * @param $countTeam
     * @param $phase
     * @return mixed
     */
    public static function checkRealPhase($countTeam, $phase){
        if($countTeam <= pow(2,($phase-1))){
            $phase--;
            return self::checkRealPhase($countTeam, $phase);
        }
        else{
            return $phase;
        }
    }

    /**
     * @return array
     */
    public static function fetchTournamentList()
    {
        $tournaments = [];

        try {
            $pdo = Database::getInstance()->getConnection();

            $stmt = $pdo->prepare("SELECT * FROM tournament");
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_ASSOC);

            foreach ($stmt->fetchAll() as $row) {
                $tournaments [] = new Tournament($row);
            }
        } catch (PDOException $err) {
            echo 'Połączenie nieudane: ' . $err->getMessage();
        }

        return $tournaments;
    }

    /**
     * Tournament constructor.
     * @param array $row
     */
    public function __construct($row = array())
    {
        $this->id = isset($row['id']) ? $row['id'] : null;
        $this->name = isset($row['name']) ? $row['name'] : '';
        $this->status = isset($row['status']) ? $row['status'] : '';
        $this->teamAmount = isset($row['team_amount']) ? $row['team_amount'] : '';

    }

    /**
     * @param int $phase
     * @param null $incomingMatchId
     */
    public function generate($phase=0,$incomingMatchId=null)
    {
        if ($phase <= 0) {
            return;
        } else {
            $row = [
                'phase' => $phase,
                'incoming_match_id' => $incomingMatchId,
                'tournament_id' => $this->id
            ];
            $match = new Match($row);
            $match->createMatch();
            $phase--;
            $this->generate($phase, $match->getId());
            $this->generate($phase, $match->getId());
            $this->status = isset($row['status']) ? $row['status'] : '';

        }
    }

    /**
     *
     */
    public function begin()
    {
        try {
            $pdo = Database::getInstance()->getConnection();
            $sql = "UPDATE `tournament` SET `status`=1 WHERE id=:id";
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue(':id', $this->getId(), PDO::PARAM_INT);
            $stmt->execute();
        } catch (PDOException $err) {
            echo 'Połączenie nieudane: ' . $err->getMessage();
        }
    }

    /**
     *
     */
    public function save()
    {
        try {
            $pdo = Database::getInstance()->getConnection();

            if (!isset($this->id)) {
                $sql = "INSERT INTO tournament(name, team_amount, status) VALUES(:name, :teamAmount, 0)";

                $stmt = $pdo->prepare($sql);

                $stmt->bindValue(':name', $this->getName(), PDO::PARAM_STR);
                $stmt->bindValue(':teamAmount', $this->getTeamAmount(), PDO::PARAM_INT);

                $stmt->execute();

                $this->setId($pdo->lastInsertId());
            }
        } catch (\PDOException $exception) {
            // TODO: log database errors
            throw $exception;
        }
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getTeamAmount()
    {
        return $this->teamAmount;
    }

    /**
     * @param mixed $teamAmount
     */
    public function setTeamAmount($teamAmount)
    {
        $this->teamAmount = $teamAmount;
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
}