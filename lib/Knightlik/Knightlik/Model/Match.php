<?php


namespace Knightlik\Knightlik\Model;

use Knightlik\Knightlik\Util\Database;
use PDO;
use PDOException;

class Match
{
    /**
     * @var integer
     */
    protected $id = null;
    /**
     * @var integer
     */
    protected $idTeam1 = null;
    /**
     * @var integer
     */
    protected $idTeam2 = null;
    /**
     * @var integer
     */
    protected $winnerTeam = null;
    /**
     * @var integer
     */
    protected $incomingMatchId = null;
    /**
     * @var integer
     */
    protected $tournamentId = null;
    /**
     * @var integer
     */
    protected $teamId = null;
    /**
     * @var integer
     */
    protected $phase = null;

    /**
     * @param $id
     * @return array
     */
    public static function fetchTeamsByTournamentId($id)
    {
        {
            $team = [];
            try {
                $pdo = Database::getInstance()->getConnection();

                $stmt = $pdo->prepare("SELECT team_id FROM team_tournament WHERE tournament_id=:id");
                $stmt->bindValue(':id', $id, PDO::PARAM_INT);        //id tournament

                $stmt->execute();
                $stmt->setFetchMode(PDO::FETCH_ASSOC);

                foreach ($stmt->fetchAll() as $row) {
                    $team [] = $row;
                }
            } catch (PDOException $exception) {
                // TODO: log database errors
                throw $exception;
            }

            return $team;
        }
    }

    /**
     * @param $idTournament
     * @return mixed
     */
    public static function fetchMaxPhase($idTournament)
    {
        {
            try {
                $pdo = Database::getInstance()->getConnection();

                $stmt = $pdo->prepare("SELECT MAX(phase) as faza FROM `match` WHERE tournament_id=:tournamentId");
                $stmt->bindValue(':tournamentId', $idTournament, PDO::PARAM_INT);

                $stmt->execute();
                $stmt->setFetchMode(PDO::FETCH_ASSOC);

                $maxPhase = $stmt->fetch();
                $maxPhase = $maxPhase['faza'];
            } catch (PDOException $exception) {
                // TODO: log database errors
                throw $exception;
            }

            return $maxPhase;
        }
    }

    /**
     * @param $phase
     * @param $tournamentId
     * @return array
     */
    public static function fetchMatchesByPhase($phase, $tournamentId)
    {
        $matches = [];

        try {
            $pdo = Database::getInstance()->getConnection();

            $stmt = $pdo->prepare("SELECT * FROM `match` WHERE phase = :phase AND tournament_id = :tournament_id");
            $stmt->bindValue(':phase', $phase, PDO::PARAM_INT);
            $stmt->bindValue(':tournament_id', $tournamentId, PDO::PARAM_INT);

            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_ASSOC);

            foreach ($stmt->fetchAll() as $row) {
                $matches [] = new Match($row);
            }
        } catch (PDOException $err) {
            echo 'Połączenie nieudane: ' . $err->getMessage();
        }

        return $matches;
    }

    /**
     * @param $tournamentId
     * @return array
     */
    public static function fetchAllMatchesByTournamentId($tournamentId)
    {
        $matches = [];

        try {
            $pdo = Database::getInstance()->getConnection();

            $stmt = $pdo->prepare("SELECT * FROM `match` WHERE tournament_id = :tournament_id ORDER BY phase DESC");
            $stmt->bindValue(':tournament_id', $tournamentId, PDO::PARAM_INT);

            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_ASSOC);

            foreach ($stmt->fetchAll() as $row) {
                $matches [] = new Match($row);
            }
        } catch (PDOException $err) {
            echo 'Połączenie nieudane: ' . $err->getMessage();
        }

        return $matches;
    }

    /**
     * Match constructor.
     * @param array $row
     */
    public function __construct($row = array())
    {
        $this->id = isset($row['id']) ? $row['id'] : null;
        $this->idTeam1 = isset($row['id_team1']) ? $row['id_team1'] : '';
        $this->idTeam2 = isset($row['id_team2']) ? $row['id_team2'] : '';
        $this->winnerTeam = isset($row['winner_team']) ? $row['winner_team'] : '';
        $this->phase = isset($row['phase']) ? $row['phase'] : '';
        $this->incomingMatchId = isset($row['incoming_match_id']) ? $row['incoming_match_id'] : '';
        $this->tournamentId = isset($row['tournament_id']) ? $row['tournament_id'] : '';
        $this->teamId = isset($row['team_id']) ? $row['team_id'] : '';
    }

    /**
     * @param $teamId
     * @param $matchId
     */
    public function setTeam1($teamId, $matchId)
    {
        try {
            $pdo = Database::getInstance()->getConnection();

            $sql = "UPDATE `match` SET id_team1 = :team1 WHERE `phase` = 1 AND `id` = :id";
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue(':team1', $teamId, PDO::PARAM_INT);
            $stmt->bindValue(':id', $matchId, PDO::PARAM_INT);

            $stmt->execute();

        } catch (PDOException $exception) {
            // TODO: log database errors
            throw $exception;
        }
    }

    /**
     * @param $teamId
     * @param $matchId
     */
    public function setTeam2($teamId, $matchId)
    {
        try {
            $pdo = Database::getInstance()->getConnection();

            $sql = "UPDATE `match` SET `id_team2` = :team2 WHERE `phase` = 1 AND `id` = :id";
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue(':team2', $teamId, PDO::PARAM_INT);
            $stmt->bindValue(':id', $matchId, PDO::PARAM_INT);

            $stmt->execute();

        } catch (PDOException $exception) {
            // TODO: log database errors
            throw $exception;
        }
    }

    /**
     * @return $this
     */
    public function createMatch()
    {
        try {
            $pdo = Database::getInstance()->getConnection();

            $sql = "INSERT INTO `match`(incoming_match_id, phase, tournament_id) VALUES(:incoming_match_id,:phase,:tournament_id)";


            $stmt = $pdo->prepare($sql);

            $stmt->bindValue(':incoming_match_id', $this->getIncomingMatchId(), PDO::PARAM_INT);
            $stmt->bindValue(':phase', $this->getPhase(), PDO::PARAM_INT);
            $stmt->bindValue(':tournament_id', $this->getTournamentId(), PDO::PARAM_INT);
            $stmt->execute();

            $this->setId($pdo->lastInsertId());

        } catch (\PDOException $exception) {
            // TODO: log database errors
            throw $exception;
        }

        return $this;

    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getIdTeam1()
    {
        return $this->idTeam1;
    }

    /**
     * @param int $idTeam1
     */
    public function setIdTeam1($idTeam1)
    {
        $this->idTeam1 = $idTeam1;
    }

    /**
     * @return int
     */
    public function getIdTeam2()
    {
        return $this->idTeam2;
    }

    /**
     * @param int $id_team2
     */
    public function setIdTeam2($id_team2)
    {
        $this->idTeam2 = $id_team2;
    }

    /**
     * @return int
     */
    public function getWinnerTeam()
    {
        return $this->winnerTeam;
    }

    /**
     * @param int $winnerTeam
     */
    public function setWinnerTeam($winnerTeam)
    {
        $this->winnerTeam = $winnerTeam;
    }

    /**
     * @return string
     */
    public function getPhase()
    {
        return $this->phase;
    }

    /**
     * @param string $phase
     */
    public function setPhase($phase)
    {
        $this->phase = $phase;
    }

    /**
     * @return int
     */
    public function getIncomingMatchId()
    {
        return $this->incomingMatchId;
    }

    /**
     * @return int
     */
    public function getTournamentId()
    {
        return $this->tournamentId;
    }

    /**
     * @param int $incomingMatchId
     */
    public function setIncomingMatchId($incomingMatchId)
    {
        $this->incomingMatchId = $incomingMatchId;
    }

    /**
     * @param int $tournamentId
     */
    public function setTournamentId($tournamentId)
    {
        $this->tournamentId = $tournamentId;
    }

}



