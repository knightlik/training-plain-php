<?php

namespace Knightlik\Knightlik\Model;

use Knightlik\Knightlik\Util\Database;
use PDO;


class Participant
{
    /**
     * @var integer
     */
    protected $id = null;

    /**
     * @var integer
     */
    protected $teamId = null;

    /**
     * @var string
     */
    protected $firstName;

    /**
     * @var string
     */
    protected $lastName;

    /**
     * @var string
     */
    protected $nickName;

    /**
     * @var intiger
     */
    protected $role;


    /**
     * @param array $row
     */
    public function __construct($row = array()) {
        $this->id = isset($row['id']) ? $row['id'] : null;
        $this->firstName = isset($row['leaderFirstName']) ? $row['leaderFirstName'] : '';
        $this->lastName = isset($row['leaderLastName']) ? $row['leaderLastName'] : '';
        $this->nickName = isset($row['leaderNickName']) ? $row['leaderNickName'] : '';
        $this->role = isset($row['role']) ? $row['role'] : null;
    }

    /**
     * Inserts new team or updates existing one basing on team ID value
     */
    public function save() {
        try {
            $pdo = Database::getInstance()->getConnection();

            if (!isset($this->id)) {
                $sql = "INSERT INTO participant( team_id,first_name, last_name, nick_name, role) VALUES(:team_id,:first_name, :last_name, :nick_name, :role)";

                $stmt = $pdo->prepare($sql);

                $stmt->bindValue(':team_id', $this->getTeamId());
                $stmt->bindValue(':first_name', $this->getFirstName());
                $stmt->bindValue(':last_name', $this->getLastName());
                $stmt->bindValue(':nick_name', $this->getNickName());
                $stmt->bindValue(':role', $this->getRole());

                $stmt->execute();

                $this->setId($pdo->lastInsertId());
            } else {
             //   TODO: Update
                $sql = "UPDATE team SET name = :name, tag = :tag, level = :level WHERE id = :id";

                $stmt = $pdo->prepare($sql);

                $stmt->bindValue(':id', $this->getId());
                $stmt->bindValue(':name', $this->getName());
                $stmt->bindValue(':tag', $this->getTag());
                $stmt->bindValue(':level', $this->getLevel());

                $stmt->execute();
            }
        } catch (\PDOException $exception) {
            // TODO: log database errors
            throw $exception;
        }
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getNickName()
    {
        return $this->nickName;
    }

    /**
     * @param string $nickName
     */
    public function setNickName($nickName)
    {
        $this->nickName = $nickName;
    }

    /**
     * @return int
     */
    public function getTeamId()
    {
        return $this->teamId;
    }

    /**
     * @param int $teamId
     */
    public function setTeamId($teamId)
    {
        $this->teamId = $teamId;
    }

    /**
     * @return intiger
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param intiger $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }


}
