<?php

namespace Knightlik\Knightlik\Model;

use Knightlik\Knightlik\Util\Database;
use PDO;


class Team
{
    /**
     * @var integer
     */
    protected $id = null;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $tag;

    /**
     * @var string
     */
    protected $level;

    /**
     * @return array
     */
    public static function fetchTeams()
    {
        $teams = [];

        try {
            $pdo = Database::getInstance()->getConnection();

            $stmt = $pdo->prepare("SELECT * FROM team");
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_ASSOC);

            // TODO: upewnić się co zwróci fetch all w przypadku braku wyników
            foreach ($stmt->fetchAll() as $row) {
                $teams[] = new Team($row);
            }

        } catch (PDOException $err) {
            echo 'Połączenie nieudane: ' . $err->getMessage();
        }

        return $teams;
    }
    public static function fetchTeamById($id)
    {
        $teams = [];

        try {
            $pdo = Database::getInstance()->getConnection();

            $stmt = $pdo->prepare("SELECT * FROM team WHERE id=:id");

            $stmt->bindValue(':id', $id, PDO::PARAM_STR);
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $team = new Team($stmt->fetch());

        } catch (PDOException $err) {
            echo 'Połączenie nieudane: ' . $err->getMessage();
        }

        return $team;
    }

    /**
     * @param array $row
     */
    public function __construct($row = array())
    {
        $this->id = isset($row['id']) ? $row['id'] : null;
        $this->name = isset($row['name']) ? $row['name'] : '';
        $this->tag = isset($row['tag']) ? $row['tag'] : '';
        $this->level = isset($row['level']) ? $row['level'] : null;
    }

    /**
     * Inserts new team or updates existing one basing on team ID value
     */
    public function save()
    {
        try {
            $pdo = Database::getInstance()->getConnection();

            if (!isset($this->id)) {
                $sql = "INSERT INTO team(name, tag, level) VALUES(:name, :tag, :level)";

                $stmt = $pdo->prepare($sql);

                $stmt->bindValue(':name', $this->getName(), PDO::PARAM_STR);
                $stmt->bindValue(':tag', $this->getTag(), PDO::PARAM_STR);
                $stmt->bindValue(':level', $this->getLevel(), PDO::PARAM_INT);

                $stmt->execute();

                $this->setId($pdo->lastInsertId());
            } else {
                $sql = "UPDATE team SET name = :name, tag = :tag, level = :level WHERE id = :id";

                $stmt = $pdo->prepare($sql);

                $stmt->bindValue(':id', $this->getId());
                $stmt->bindValue(':name', $this->getName());
                $stmt->bindValue(':tag', $this->getTag());
                $stmt->bindValue(':level', $this->getLevel());

                $stmt->execute();
            }
        } catch (\PDOException $exception) {
            // TODO: log database errors
            throw $exception;
        }
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param string $tag
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
    }

    /**
     * @return string
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param string $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }
}
