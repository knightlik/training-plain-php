<?php

namespace Knightlik\Knightlik\Validator;


class ValidatorString extends Validator
{
    /**
     * @var string
     */
    const ENCODING = 'utf-8';

    /**
     * @param $value
     * @param array $params - label, minLength, maxLength, encoding (default: utf-8)
     * @param array $messages - minLength, maxLength
     */
    public function __construct($value, $params = array(), $messages = array())
    {
        $this->checkValueExists($messages, 'minLength', "%s musi mieć co najmniej %s znaków");
        $this->checkValueExists($messages, 'maxLength', "%s musi mieć nie więcej niż %s znaków");

        $this->checkValueExists($params, 'label');
        $this->checkValueExists($params, 'minLength');
        $this->checkValueExists($params, 'maxLength');
        $this->checkValueExists($params, 'encoding', self::ENCODING);

        parent::__construct($value, $params, $messages);
    }

    protected function doValidate()
    {
        $this->clean = trim($this->value);

        if (mb_strlen($this->clean, $this->params['encoding']) < $this->params['minLength']) {
            $this->errors[] = sprintf($this->messages['minLength'], $this->params['label'], $this->params['minLength']);
        }

        if (mb_strlen($this->clean, $this->params['encoding']) > $this->params['maxLength']) {
            $this->errors[] = sprintf($this->messages['maxLength'], $this->params['label'], $this->params['maxLength']);
        }
    }
}
