<?php

namespace Knightlik\Knightlik\Validator;


abstract class Validator
{
    /**
     * @var mixed
     */
    protected $value = null;

    /**
     * @var mixed
     */
    protected $clean = null;

    /**
     * @var array
     */
    protected $errors = array();

    /**
     * @var array
     */
    protected $params = array();

    /**
     * @var array
     */
    protected $messages = array();

    /**
     * @param $value
     */
    public function __construct($value, $params = array(), $messages = array()) {
        $this->value = $value;
        $this->params = $params;
        $this->messages = $messages;
    }

    /**
     * @return boolean
     */
    public function validate() {
        $this->errors = array();

        $this->doValidate();

        $isValid = empty($this->errors);

        if (!$isValid) {
            $this->clean = null;
        }

        return $isValid;
    }

    abstract protected function doValidate();

    /**
     * @param $param
     * @param $params
     * @param $default
     */
    protected function checkValueExists(&$params, $param, $default = null) {
        if (!isset($params[$param])) {
            if (isset($default)) {
                $params[$param] = $default;
            } else {
                throw new Exception("Atrybut '{$param}' jest wymagany dla klasy walidatora " . __CLASS__);
            }
        }
    }

    /**
     * @return mixed
     */
    public function getClean()
    {
        return $this->clean;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }
}
