<?php

namespace Knightlik\Knightlik\Validator;


class ValidatorStringUpperCaseFirst extends ValidatorString
{
    protected function doValidate()
    {
        parent::doValidate();

        $this->clean = ucfirst($this->clean);
    }
}
