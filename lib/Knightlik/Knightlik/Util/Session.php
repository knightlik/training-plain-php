<?php

namespace Knightlik\Knightlik\Util;


class Session
{
    /**
     * @var string
     */
    const SESSION_IDENTIFIER = 'knightlik';

    /**
     * @var integer
     */
    const SESSION_LIFETIME = 3600;

    /**
     *
     */
    const MESSAGES_ARRAY_NAME = '17f3467db103e03ea7354dd6da9a32c1ed2a07e8';

    /**
     * @var string
     */
    const ERRORS_ARRAY_NAME = '570043596e41f9067d43fbff99f1acb348a090bf';

    public function __construct() {
        $this->create();
    }

    protected function create() {
        session_name(self::SESSION_IDENTIFIER);
        session_set_cookie_params(self::SESSION_LIFETIME);
        session_start();
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function add($name, $value) {
        $_SESSION[$name] = $value;
    }

    /**
     * @param $name
     * @param mixed $default
     *
     * @return mixed
     */
    public function get($name, $default = null) {
        return isset($_SESSION[$name]) ? $_SESSION[$name] : $default;
    }

    /**
     * @param string $name
     */
    public function remove($name) {
        if (isset($_SESSION[$name])) {
            unset($_SESSION[$name]);
        }
    }

    /**
     * @param string $name
     * @param mixed $default
     *
     * @return mixed
     */
    public function pop($name, $default) {
        $value = $this->get($name, $default);
        $this->remove($name);

        return $value;
    }

    /**
     * @return array
     */
    public function popMessages() {
        $messages = isset($_SESSION[self::MESSAGES_ARRAY_NAME]) ? $_SESSION[self::MESSAGES_ARRAY_NAME] : array();
        unset($_SESSION[self::MESSAGES_ARRAY_NAME]);

        return $messages;
    }

    /**
     * @param string $key
     *
     * @return array
     */
    public function popErrors($key) {
        $errors = isset($_SESSION[self::ERRORS_ARRAY_NAME][$key]) ? $_SESSION[self::ERRORS_ARRAY_NAME][$key] : array();
        unset($_SESSION[self::ERRORS_ARRAY_NAME][$key]);

        return $errors;
    }
}
