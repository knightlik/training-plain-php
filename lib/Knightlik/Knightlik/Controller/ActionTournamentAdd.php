<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 21.03.16
 * Time: 11:52
 */


namespace Knightlik\Knightlik\Controller;


use Knightlik\Knightlik\Model\Tournament;

use Knightlik\Knightlik\Util\Request;
use Knightlik\Knightlik\Util\Session;
use Knightlik\Knightlik\Validator\Validator;
use Knightlik\Knightlik\Validator\ValidatorStringUpperCaseFirst;
use PDO;
use PDOException;


class ActionTournamentAdd extends Action
{

    /**
     *
     */
    protected function doExecute()
    {

        $viewParameters = array();

        if ($this->request->isMethod(Request::METHOD_POST)) {
            $data = $this->request->get('tournament', array());

            $validators = [];
            $validators['name'] = new ValidatorStringUpperCaseFirst(isset($data['name']) ? $data['name'] : '', [
                'label' => 'Nazwa turnieju',
                'minLength' => 3,
                'maxLength' => 30
            ], [
                'minLength' => '%s musi mieć co najmniej %s znaki'
            ]
            );
            $validators['team_amount'] = new ValidatorStringUpperCaseFirst(isset($data['team_amount']) ? $data['team_amount'] : '', [
                'label' => 'Ilość drużyn',
                'minLength' => 1,
                'maxLength' => 3
            ], [
                'minLength' => '%s musi być potęgą liczby dwa/ %s znaki'
            ]
            );
//TODO: walidacja numerów
//            $validators['players'] = new ValidatorStringUpperCaseFirst(isset($data['players']) ? $data['players'] : '', [
//                'label' => 'Liczba graczy',
//                'badNumber8' => 8,
//                'badNumber16' => 16,
//                'badNumber32' => 32,
//                'badNumber64' => 64,
//                'badNumber128' => 128,
//            ], [
//                'minLength' => '%s musi mieć co najmniej %s znaki'
//            ]);

            $errors = array();
            foreach ($validators as $field => $validator) {
                if ($validator->validate()) {
                    $clean[$field] = $validator->getClean();
                } else {
                    $errors[$field] = $validator->getErrors();
                }
            }

            $hasErrors = false;
            foreach ($errors as $error) {
                if (!empty($error)) {
                    $hasErrors = true;
                }
            }

            if (!$hasErrors) {
                $tournament = new Tournament($clean);
                $tournament->save(); // tworzenie turnieju
                $this->session->add(Session::MESSAGES_ARRAY_NAME, array('Dodawanie turnieju drużyny zakończone sukcesem!'));

                return $this->response->redirect('?action=addTournament');
            } else {
                $viewParameters['errors'] = $errors;
                $viewParameters['data'] = $data;
            }
        }

        $content = $this->response->processTemplate('tournamentAdd', $viewParameters);
        $content = $this->response->processTemplate('layout', array(
            'title' => 'Knightlik! - Dodaj turniej',
            'content' => $content
        ));

        $this->response->setContent($content);
    }
}