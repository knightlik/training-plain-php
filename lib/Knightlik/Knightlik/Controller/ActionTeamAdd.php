<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 08.03.16
 * Time: 10:24
 */

namespace Knightlik\Knightlik\Controller;


use Knightlik\Knightlik\Model\Team;
use Knightlik\Knightlik\Model\Participant;

use Knightlik\Knightlik\Util\Request;
use Knightlik\Knightlik\Util\Session;
use Knightlik\Knightlik\Validator\ValidatorString;
use Knightlik\Knightlik\Validator\ValidatorStringUpperCaseFirst;
use PDO;
use PDOException;

class ActionTeamAdd extends Action
{
    protected function doExecute()
    {
        $viewParameters = array();

        if ($this->request->isMethod(Request::METHOD_POST)) {
            $data = $this->request->get('team', array());
            $validators = [];
            $validators['name'] = new ValidatorStringUpperCaseFirst(isset($data['name']) ? $data['name'] : '', [
                'label' => 'Nazwa',
                'minLength' => 4,
                'maxLength' => 30
            ], [
                'minLength' => '%s musi mieć co najmniej %s znaki'
            ]);
            $validators['tag'] = new ValidatorString(isset($data['tag']) ? $data['tag'] : '', [
                'label' => 'Tag',
                'minLength' => 3,
                'maxLength' => 5
            ], [
                'minLength' => '%s musi mieć co najmniej %s znaki'
            ]);
            $validators['leaderNickName'] = new ValidatorStringUpperCaseFirst(isset($data['leaderNickName']) ? $data['leaderNickName'] : '', [
                'label' => 'Nick kapitana',
                'minLength' => 3,
                'maxLength' => 20
            ], [
                'minLength' => '%s musi mieć co najmniej %s znaki'
            ]);
            $validators['leaderFirstName'] = new ValidatorStringUpperCaseFirst(isset($data['leaderFirstName']) ? $data['leaderFirstName'] : '', [
                'label' => 'Imię kapitana',
                'minLength' => 2,
                'maxLength' => 30
            ], [
                'minLength' => '%s musi mieć co najmniej %s znaki'
            ]);
            $validators['leaderLastName'] = new ValidatorStringUpperCaseFirst(isset($data['leaderLastName']) ? $data['leaderLastName'] : '', [
                'label' => 'Nazwisko kapitana',
                'minLength' => 2,
                'maxLength' => 30
            ], [
                'minLength' => '%s musi mieć co najmniej %s znaki'
            ]);

            $errors = array();
            foreach ($validators as $field => $validator) {
                if ($validator->validate()) {
                    $clean[$field] = $validator->getClean();
                } else {
                    $errors[$field] = $validator->getErrors();
                }
            }

            $hasErrors = false;
            foreach ($errors as $error) {
                if (!empty($error)) {
                    $hasErrors = true;
                }
            }

            if (!$hasErrors) {
                $team = new Team($clean);
                $team->save();
                $leader = new Participant($clean);
                $leader->setTeamId($team->getId());
                $leader->save();
                $this->session->add(Session::MESSAGES_ARRAY_NAME, array('Dodawanie drużyny zakończone sukcesem!'));

                return $this->response->redirect('?action=addTeam');
            } else {
                $viewParameters['errors'] = $errors;
                $viewParameters['data'] = $data;
            }
        }

        $content = $this->response->processTemplate('teamAdd', $viewParameters);
        $content = $this->response->processTemplate('layout', array(
            'title' => 'Knightlik! - Dodaj drużynę',
            'content' => $content
        ));

        $this->response->setContent($content);
    }
}
