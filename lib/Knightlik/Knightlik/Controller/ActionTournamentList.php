<?php
namespace Knightlik\Knightlik\Controller;


use Knightlik\Knightlik\Model\Tournament;
use Knightlik\Knightlik\Model\Match;
use Knightlik\Knightlik\Util\Request;
use Knightlik\Knightlik\Util\Session;
use PDO;
use PDOException;

class ActionTournamentList extends Action
{
    protected function doExecute()
    {

        $noTeams=false;
        if (isset($_POST['goToTree']) == true){
            header("Location: ?action=treeTournament");
            die;
        }
        if (isset($_POST['begin']) == true && isset($_POST['tournamentId'])) {
            $tournament = Tournament::fetchTournament($_POST['tournamentId']);
            $id = $_POST['tournamentId'];


            if ($tournament->getStatus() == false) {
                $teamAmount = $tournament->getTeamAmount();
                $startPhase=log((int)$teamAmount)/log(2);
                // logarytm z $this->teamAmount o podstawie 2
                $countTeam = $tournament::findAllTeams($tournament->getId());
                if (!isset($countTeam)||!is_numeric($countTeam)||$countTeam==0){
                    $noTeams=true;

                }
                else {
                    $tournament->begin();
                    $phase = $tournament::checkRealPhase($countTeam, $startPhase);
                    $tournament->generate($phase);
                }
            }
        }
        $addToTournament = isset($_POST['addToTournament'])? $_POST['addToTournament'] : null;
        if(isset($_POST['addTeamToTournament']) == true){
            foreach ($addToTournament as $singleTeam)
            Tournament::addTeamInTournament($_SESSION['teamToTournamentId'],$singleTeam);}
        $_SESSION['tournamentP'] = tournament::fetchTournamentList();
        $content = $this->response->processTemplate('tournamentList', $_SESSION['tournamentP']);
        $content = ($noTeams==true)?"Brak drużyn przypisanych do turnieju! <br/> <a class=\"linkOfButton\" href='http://php.localhost/training-plain-php/?action=listTournament'><button  class=\"button\">Wróć</button></a></br><a class=\"linkOfButton\" href='http://php.localhost/training-plain-php/?action=teamTournament&tournamentId=$id'><button class=\"button\" >Przypisz drużyny</button></a>":$content;
        $content = $this->response->processTemplate('layout', array(
            'title' => 'Knightlik! - Rozpocznij Turniej',
            'content' => $content
        ));
        $this->response->setContent($content);

    }
}