<?php

namespace Knightlik\Knightlik\Controller;


class ActionIndex extends Action
{
    protected function doExecute()
    {
        $content = "";
        $content = $this->response->processTemplate('index', $content);
        $content = $this->response->processTemplate('layout', array(
            'title' => 'Knightlik! - Menu',
            'content' => $content
        ));
        $this->response->setContent($content);
    }
}
