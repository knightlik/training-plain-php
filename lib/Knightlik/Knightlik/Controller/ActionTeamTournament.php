<?php
namespace Knightlik\Knightlik\Controller;
use Knightlik\Knightlik\Model\Team;
use Knightlik\Knightlik\Model\Participant;

use Knightlik\Knightlik\Util\Request;
use Knightlik\Knightlik\Util\Session;
use Knightlik\Knightlik\Validator\ValidatorString;
use Knightlik\Knightlik\Validator\ValidatorStringUpperCaseFirst;
use PDO;
use PDOException;
class ActionTeamTournament extends Action
{
    protected function doExecute() {



        $_SESSION['nrOfPage'] = isset($_SESSION['nrOfPage']) && is_numeric($_SESSION['nrOfPage']) && $_SESSION['nrOfPage']>=1 ? $_SESSION['nrOfPage'] : 1 ;
        $_SESSION['nrOfResults'] = 3 ;

        $this->response->setHeader('action','teamTournament');
        $_SESSION['teamParam']=Team::fetchTeams( $_SESSION['nrOfPage'], $_SESSION['nrOfResults'] );

        $this->response->setContent('Wyświetlanie drużyn');

        $content = $this->response->processTemplate('teamTournament', $_SESSION['teamParam']);
        $content = $this->response->processTemplate('layout', array(
            'title' => 'Knightlik! - Przypisz drużynę do turnieju',
            'content' => $content
        ));

        $this->response->setContent($content);

    }

}