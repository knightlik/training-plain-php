<?php

namespace Knightlik\Knightlik\Controller;


use Knightlik\Knightlik\Model\Match;
use Knightlik\Knightlik\Model\Tournament;
use Knightlik\Knightlik\Model\Team;
use Knightlik\Knightlik\Util\Request;
use Knightlik\Knightlik\Util\Session;
use PDO;
use PDOException;


class ActionTournamentTree extends Action
{

    protected function doExecute()
    {
        $tournamentId = $_POST['tournamentId'];
        $teams = Match::fetchTeamsByTournamentId($tournamentId);
        $team = shuffle($teams);
        $matches = Match::fetchMatchesByPhase(1, $tournamentId);

        foreach ($matches as $match) {
            if ($match->getIdTeam1() == null) {
                if (!empty($team)) {
                    $matchId = $match->getId();
                    $team = array_shift($teams);
                    $match->setTeam1($team['team_id'], $matchId);
                } else {
                    break;
                }
            }
        }

        foreach ($matches as $match) {
            if ($match->getIdTeam2() == null) {
                if (!empty($team)) {
                    $matchId = $match->getId();
                    $team = array_shift($teams);
                    $match->setTeam2($team['team_id'], $matchId);
                } else {
                    break;
                }
            }
        }

        $matches = Match::fetchAllMatchesByTournamentId($tournamentId);
        $matches = array_reverse($matches);
        $tournament = Tournament::fetchTournament($tournamentId);
        $teamAmount = $tournament->getTeamAmount();
        $currentPhase = null;
        $content = "<div id='allPhases'><div>";
        foreach ($matches as $id => $match) {
            $team1Id = Team::fetchTeamById($match->getIdTeam1());
            $team2Id = Team::fetchTeamById($match->getIdTeam2());
            $team1Tag = $team1Id->getTag();
            $team2Tag = $team2Id->getTag();
            if ($match->getPhase() != $currentPhase) {
                $content = $content . "</div>";
                $topMargin = isset($topMargin) ? 100 / $teamAmount : "none";
                $teamAmount = $teamAmount / 2;
                $content = $content . "<div class='phases' id='phase" . $match->getPhase() . "'>";
                $content = $content . "</br><div class='matches' id='match" . $match->getId() . " 'style='margin-top:$topMargin%'>";
                $content = !empty($team1Tag)?$content . $team1Tag . " ":$content . "Nobody ";
                $content = $content . "VS. ";
                $content = !empty($team2Tag)?$content . $team2Tag . " ":$content . "Nobody ";
                $content = $content . "</div>";

                $currentPhase = $match->getPhase();
            } else {
                $content = $content . "<br/><div class='matches' id='match" . $match->getId() . " 'style='margin-top:$topMargin%'>";
                $content = !empty($team1Tag)?$content . $team1Tag . " ":$content . "Nobody ";
                $content = $content . "VS. ";
                $content = !empty($team2Tag)?$content . $team2Tag . " ":$content . "Nobody ";
                $content = $content . "</div>";
            }
        }
        $content = $content . "<div class='clear'></div></div></div>";

        $content = $this->response->processTemplate('tournamentTree', $content);
        $content = $this->response->processTemplate('layout', array(
            'title' => 'Knightlik! - Drzewko Turniejowe',
            'content' => $content
        ));
        $this->response->setContent($content);
    }
}