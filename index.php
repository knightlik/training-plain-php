<?php

require_once "autoload.php";

use Knightlik\Knightlik\Controller\Controller;


$controller = new Controller();
$controller->run();
