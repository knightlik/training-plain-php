<?php

require_once 'lib/Knightlik/Knightlik/Autoload/Autoloader.php';

use Knightlik\Knightlik\Autoload\Autoloader;


$autoloader = new Autoloader(__DIR__ . DIRECTORY_SEPARATOR . 'lib');

spl_autoload_register(array($autoloader, 'autoload'));
